/**
 * 
 */
package in.xpeditions.user.entity;

/**
 * @author richardmtp@gmail.com
 *
 */
public class User {
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
