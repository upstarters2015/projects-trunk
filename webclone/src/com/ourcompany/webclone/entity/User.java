/**
 * 
 */
package com.ourcompany.webclone.entity;

/**
 * @author richardmtp@gmail.com
 *
 */
public class User {
	private long id;
	private long userId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
}
